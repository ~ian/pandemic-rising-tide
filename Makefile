# Makefile - build system for new board layouts for
#   for games very like Pandemic Rising Tide
#
# Copyright (C) 2019 Ian Jackson
#
# This program is dual licensed, GPv3+ or CC-BY-SA 4.0+.
# Only to the Pandemic Rising Tide folks, it is permissively licensed.
#
#   This program is free software.
#
#   You can redistribute it and/or modify it under the terms of the
#   GNU General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version; or (at your option), under the terms of the
#   Creative Commons Attribution-ShareAlike International License,
#   version 4.0 of that License, or (at your option), any later
#   version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License Creative Commons Attribution-ShareAlike
#   License or the for more details.
#
#   You should have received a copy of these licenses along with this
#   program.  If not, see <https://www.gnu.org/licenses/> and
#   <https://creativecommons.org/licenses/>.
#
# Pandemic and Pandemic Rising Tide are (I think) trademarks of Z-Man
# games and I use them without permission.
# 
# For the avoidance of doubt, I do not consider this program to be a
# derivative work of the game Pandemic Rising Tide.  However, it is
# not very useful without a pair of game description files and the
# only nontrivial game description files I know of are indeed such
# derivatives.

SHELL=/bin/bash

i=mv -f $@.tmp $@
o= >$@.tmp && $i

export PERL_HASH_SEED=1

# PLAG_DEBUG   ?= $(PLAG)     # effectively
# PLAG_RELEASE ?= $(PLAG)     # ie caller may set these
PLAG_DIR     ?= ../plag-mangler

USE_PLAG_DEBUG   := $(shell ./find-plag \
 debug:release "$(PLAG_DIR)" "$(PLAG_DEBUG)" "$(PLAG_RELEASE)" "$(PLAG)")
USE_PLAG_RELEASE := $(shell ./find-plag \
 release:debug "$(PLAG_DIR)" "$(PLAG_RELEASE)" "$(PLAG_DEBUG)" "$(PLAG)")

# with
#  plag-mangler   3a1eae4d6395f7d862cf66d0e4a1008d1bedc6e7
#  rc-dlist-deque 3e19e16dde436fc1ac03534bf502588d17b3faeb
#  c_vec          3394762a6221c44f8e22b222ce5547f6534129ea
#  rust-nlopt     d6781621e72bb7a78da56689ab5bb27befafec6a
#  nlopt (hack)   3068d8b694a6047c4f374b7267a54fdb3fae1f69 2.3+252-g8b3cb5a-1
#  crates from Debian buster:
#    libc   0.2.43-1
#    arrayvec 0.4.8-1
#  also relevant
#    libboost-dev:amd64       1.67.0.1
#    libboost1.67-dev:amd64   1.67.0-11

PS2PDF ?= ps2pdf
PS2PDF_FLAGS ?=

export PERL_HASH_SEED=1
export PERL_PERTURB_KEYS=0

default: all

# board-pMAXPAPER-bPAGE.pdf
#   where MAXPAPER is a3 for one a4 and one a3 page, PAGEs are B, MT
#                     a4 for three a4 pages, PAGEs are B, M, T
#                     a1 for one a1 page (with much whitespace), page is P
#                     a1m like a1 but assumes pasting onto 4 x a4 boards
#
# boundings-* (one per MAXPAPER) shows various regions
#   assuming 5.5mm unprintable border, see $max_printeredge

BOARDFILES=						\
	$(addprefix maxprintable-, 	a3)		\
	$(addprefix minprintable-, 	a3) 		\
	$(addprefix boundings-, pa3 pa4 pa1-bPageP pa1m pa1m-bPageP	) \
	$(addprefix board-, 	pa3-bPageB pa3-bPageMT			\
				pa4-bPageB pa4-bPageM pa4-bPageT	\
				pa1-bPageP pa1m-bPageP			) \
	$(addprefix a4-test-, 		b m t a1m)	\
	layout-test-preview				\
	$(addprefix layout-test-, 	b m t a1m)


all:		all-ps all-pdf all-preview

PSS=$(addsuffix .ps,  $(BOARDFILES))
PDFS=$(addsuffix .pdf, $(BOARDFILES))
PREVIEWS=maxprintable-a3-preview.png
TEXTFILES=README map.plag

all-ps: 	$(PSS)
all-pdf:	$(PDFS)
all-preview:	$(PREVIEWS)

%.pdf: %.ps Makefile
	$(PS2PDF) $(PS2PDF_FLAGS) $< $@.tmp && $i

cmyk.pdf: PS2PDF_FLAGS += -dAutoRotatePages=/None

map.plag: generate-plag input-graph Parse.pm 
	./$< input-graph $o

www=ianmdlvl@chiark:public-html/pandemic-rising-tide

html: $(PDFS) $(PREVIEWS) $(TEXTFILES)
	rm -rf $@ $@.tmp; mkdir $@.tmp
	cp $^ $@.tmp/
	set -e; for f in $(TEXTFILES); do mv $@.tmp/$$f{,.txt}; done
	$i

to-www: html
	git-push
	rsync -aH --delete html/. $(www)/.

# create opt.plag.reuse to shortcut this
opt.plag: maybe-rerun-optim map.plag $(USE_PLAG_RELEASE) Makefile
	./$(filter-out Makefile, $^) $@ .opt.plag.sums \
		R					\
		DUAL					\
		OUTER-F2V OUTER-SPLIT			\
		B T OUTER-F2V OUTER-F12VA		\
		PCO CP RAE 				\
		D 0 NLOPT				\
		W $@.tmp

faces.plag: opt.plag $(USE_PLAG_DEBUG) Makefile
	$(USE_PLAG_DEBUG) RF $< W-FACES $@.tmp && $i

BOARDDEPS= generate-board faces.plag input-graph misc-data.pl Parse.pm

maxprintable-%.ps: $(BOARDDEPS)
	./generate-board -Xp$* faces.plag input-graph $o

minprintable-%.ps: $(BOARDDEPS)
	./generate-board -Xp$*,bPrintableAll faces.plag input-graph $o

%-preview.png: %.pdf Makefile
	gm convert -geometry 20% $< png:$@.tmp && $i

boundings-%.ps: $(BOARDDEPS)
	./generate-board -XB$* faces.plag input-graph $o

board-%.ps: $(BOARDDEPS)
	./generate-board -X$* faces.plag input-graph $o

layout-test-a1m.ps: $(BOARDDEPS) layout-test-prefix-a1.ps
	./generate-board -XWB,pa1m-bPageP faces.plag input-graph >$@.tmp.1
	cat layout-test-prefix-a1.ps $@.tmp.1 $o

a4-test-a1m.ps: $(BOARDDEPS) layout-test-prefix-a1.ps
	./generate-board -XB,pa1m-bPageP faces.plag input-graph >$@.tmp.1
	cat layout-test-prefix-a1.ps $@.tmp.1 $o

layout-test-preview.ps: $(BOARDDEPS) 
	./generate-board -XW faces.plag input-graph $o

layout-test-%.ps: layout-test-prefix-%.ps layout-test-preview.ps
	cat $^ $o

a4-test-%.ps: layout-test-prefix-%.ps maxprintable-a3.ps
	cat $^ $o

#map.ps: map.dot
#	neato -Tps $^ $o
