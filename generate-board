#!/usr/bin/perl -w
#
# generate-board - program for generating playing board images
#   for games very like Pandemic Rising Tide
#
# Copyright (C) 2019 Ian Jackson
#
# This program is dual licensed, GPv3+ or CC-BY-SA 4.0+.
# It copies bits of itself into its output; those parts are CC-BY 4.0+.
# Only to the Pandemic Rising Tide folks, it is permissively licensed.
#
#   This program is free software.
#
#   You can redistribute it and/or modify it under the terms of the
#   GNU General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version; or (at your option), under the terms of the
#   Creative Commons Attribution-ShareAlike International License,
#   version 4.0 of that License, or (at your option), any later
#   version.
#
#   This program copies pieces itself into the output.  Those pieces,
#   when copied into the output, are additionally (at your option)
#   available under a difference licence: they are made available
#   under the Creative Commons Attribution International License,
#   version 4.0 of that License, or (at your option), any later
#   version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License Creative Commons Attribution-ShareAlike
#   License or the for more details.
#
#   You should have received a copy of these licenses along with this
#   program.  If not, see <https://www.gnu.org/licenses/> and
#   <https://creativecommons.org/licenses/>.
#
# Pandemic and Pandemic Rising Tide are (I think) trademarks of Z-Man
# games and I use them without permission.
# 
# For the avoidance of doubt, I do not consider this program to be a
# derivative work of the game Pandemic Rising Tide.  However, it is
# not very useful without a pair of game description files and the
# only nontrivial game description files I know of are indeed such
# derivatives.


use strict;
use Carp;
use Data::Dumper;
use Math::GSL::Vector qw/:all/;
use Math::GSL::Matrix qw/:all/;
use Math::GSL::Const;
use Math::GSL::BLAS qw/:all/;
use Math::GSL::CBLAS qw/:all/;
use Math::GSL::Machine qw/:all/;

use POSIX qw(M_PI);

BEGIN { unshift @INC, qw(.); }

use Parse;

our $xopts = '';
our $bounding = 'Entire';
our $facesf;
our %vxname2pos; # $vxname2pos{VXNAME} = Math::GSL::Vector
our %bb; # $r{Bounding...}[0..3] = points
our $maxpaper = 'a3';
my $a1sfactor = 1/sqrt(8);

sub TAU { M_PI * 2.0; }
sub MM2PT { 72.0 / 25.4; }

our $mid;

our $a4_w = 595;
our $a4_h = 842;
our $a3_h = 1190;
our $a1_w = 1684;
our $a1_h = 2384;

our $max_printeredge = 5.50 * MM2PT;
our $a1m_pasteedge = 3.5 * MM2PT; # along each edge
our $preamble_from_boundings = '';
our %adjuncts_dy_from_boundings = (Top => 0, Bottom => 0);

# ----- region names from plag, incl. reverse mapping -----

our %prs2region;

sub prep_region_rmap () {
  foreach my $rn (keys %region) {
    my $prs = plag_prs($rn);
    die if $prs2region{$prs};
    $prs2region{$prs} = $rn;
  }
}

our $sre = qr{(?:(\")|(?=\w))(.*)\1$}; # 2 captures, $2 is the name

sub prs2r ($) {
  # bodgery for the sea and land
  return $c{Sea} if m/ \| / && !grep { !m{^$c{OuterfaceRe}$} } split / \| /, $_[0];
  $prs2region{$_[0]} // confess "@_ ?"
}
sub prs2rr ($) { $region{prs2r($_[0])} }

#----- file reader for plag output -----

sub read_faces () {
  # Sets
  #  $region{NAME}{Adj}[]{Ends}[]{VxName}
  #  $region{NAME}{Adj}[]{Ends}[]{Pos}
  #  $region{NAME}{Polygon}{Pos}
  #  $region{NAME}{Special}
  #
  # also incidentally
  #  $region{NAME}{Adj}[]{Ends}[]{Adjoins}
  #  $region{NAME}{Polygon}{Adjoins}
  # which should be ignored

  open P, "<", $facesf or die $!;
  while (<P>) { last if m/^\&faces$/; }
  my $rr;
  my @edges;
  my $process_rr = sub {
    my $last_ai;
    if ($rr->{Name} eq $c{Sea}) {
      # We combined land and sea; now we must split them again
      # in fact, we just throw away all L entirely.
      # Here, when processing sea, we keep only vertices that
      # are part of the sea.
      @edges = grep {
	my $evxname = $_->{VxName};
	my @eregions = split / \| ?/, $evxname;
	grep { $_ eq $c{Sea} } @eregions
      } @edges;
    }
    $rr->{Polygon} = [ @edges ];
    if ($rr->{Name} ne $c{Sea}) {
      for my $ei (0..$#edges) {
	if (!defined $last_ai) {
	  my $ai;
	  for my $ai (0..$#{ $rr->{Adj} }) {
	    next unless $rr->{Adj}[$ai]{Name} eq $edges[$ei]{Adjoins};
	    $last_ai = $ai+1;
	  }
	  confess $edges[$ei]{Adjoins}.' ?' unless defined $last_ai;
	}
	my $ai = ($last_ai-1+@edges) % @edges;
	$last_ai = $ai;
	my $adj = $rr->{Adj}[$ai];
	confess Dumper($adj->{Name}, $edges[$ei]{Adjoins},
		       $rr, \@edges, $ei, $adj, $last_ai, $ai)." ?"
	    unless $adj->{Name} eq $edges[$ei]{Adjoins};
      
	for my $endi (0..1) {
	  $adj->{Ends}[$endi] = $edges[ ($ei + $endi) % @edges ];
	}
      }
    }
    @edges = ();
    $rr = undef;
  };
  my $vxname;
  for (;;) {
    $!=0; $_=<P> // confess $!;
    last if m/^\&$/;
    if (m/^$sre$/) {
      my $new_face= $2;
      $process_rr->() if $rr;
      $rr= prs2rr($new_face);
    } elsif (m/^\s+$sre$/) {
      confess unless $rr;
      $vxname = $2;
      push @edges, { VxName => $vxname };
    } elsif (m/^\s+\^adjoins\s+$sre$/) {
      $edges[-1]{Adjoins} = prs2r($2);
    } elsif (m/^\s+\^\@([-e.0-9]+)\,([-e.0-9]+)$/) {
      my $pos = mkvec($1,$2);
      confess unless defined $vxname;
      $edges[-1]{Pos} = $pos;
      $vxname2pos{$vxname} = $pos;
    }
  }
  $process_rr->();

  $region{$_}{Special} = 1 foreach @{ $c{SpecialRegions} };
}

#----- geometry mangling -----

sub adj_ends ($) {
  my ($adj) = @_;
  map { $_->{Pos} } @{ $adj->{Ends} };
}

sub calculate_centres () {
  # Sets
  #  $region{NAME}{Centre}
  foreach my $rr (values %region) {
    next if $rr->{Special};
    my $sum = Math::GSL::Vector->new(2);
    my $wsum = 0;
    foreach my $adj (@{ $rr->{Adj} }) {
      my @ends = adj_ends($adj);
      my $w = ($ends[1] - $ends[0])->norm();
      $sum += $w * $_ foreach @ends;
      $wsum += $w;
    }
    $rr->{Centre} = $sum * (0.5 / $wsum);
  }
}

sub for_each_pos ($) {
  my ($f) = @_;
  my $call = sub {
    my ($pr,$rr,$why) = @_;
    return unless defined $$pr;
    $f->($pr,$rr,$why);
  };
  foreach my $rr (values %region) {
    $call->( \ $rr->{Centre}, $rr, $rr->{Name}." C" );
    foreach my $vertex (@{ $rr->{Polygon} }) {
      $call->( \ $vertex->{Pos}, $rr, $rr->{Name}." | ".$vertex->{Adjoins} );
    }
  }
}

sub prvec ($) {
  my ($v) = @_;
  confess unless $v;
  sprintf "%g,%g", $v->get(0), $v->get(1);
}

sub mkvec ($$) { Math::GSL::Vector->new(\@_) }
sub vec2atan ($) { my ($dir) = @_; atan2 $dir->get(1), $dir->get(0); }

sub transform_coordinates () {
  # Adjusts coordinates in graph to be [0,0] .. top right (scaled)
  # until it's all in PostScript points
  my @or = map { $region{$_}{Centre} } @{ $c{OrientRegions} };
  my $dir = $or[1] - $or[0];
  my $theta = vec2atan($dir);
  my $rotateby = (90. - $c{OrientBearing}) * ((TAU)/360.) - $theta;
  my $s = sin($rotateby);
  my $c = cos($rotateby);
  my $transform = Math::GSL::Matrix->new(2,2);
  $transform->set_row(0, [  $c, -$s ]);
  $transform->set_row(1, [  $s,  $c ]);
  #  print STDERR "rotate was=",prvec($dir)," theta=$theta",
  #    " rotateby=$rotateby s=$s c=$c\n";
  my @lims;
  foreach my $topend (qw(0 1)) {
    my $v = $topend ? -$GSL_DBL_MAX : $GSL_DBL_MAX;
    $lims[$topend] = mkvec($v,$v);
  }
  for_each_pos(sub {
    my ($pr, $rr, $why) = @_;
    my $y = Math::GSL::Vector->new(2);
    gsl_blas_dgemv($CblasNoTrans,
		   1.0, $transform->raw,
		   $$pr->raw,
		   0, $y->raw)
	and confess;
    #print STDERR "gsl_blas_dgemv ",prvec($$pr)," => ",prvec($y),"\n";
    gsl_blas_dcopy($y->raw, $$pr->raw)
	and confess;
    foreach my $topend (qw(0 1)) {
      foreach my $xy (qw(0 1)) {
	my $now = $y->get($xy);
	my $lim = $lims[$topend]->get($xy);
	#print STDERR "?set $topend $xy $now $lim\n";
	next if $topend ? ($now <= $lim) : ($now >= $lim);
	$lims[$topend]->set([$xy], [$now]);
	#print STDERR "set $topend $xy $now\n";
      }
    }
  });
  #print STDERR "lims ",prvec($lims[0])," .. ",prvec($lims[1]),"\n";
  my $translate = -$lims[0];
  #print STDERR "translate ",prvec($translate),"\n";
  my $scale = $c{GraphScale} * MM2PT;
  for_each_pos(sub {
    my ($pr) = @_;
    gsl_vector_add($$pr->raw, $translate->raw) and confess;
    gsl_vector_scale($$pr->raw, $scale) and confess;
  });
  $mid = ($lims[1] - $lims[0]) * 0.5;
}

sub pos_plus_dy_adjunct ($) {
  my ($cval) = @_;
  my ($x,$y,$whadjunct) = @$cval;
  my $adjunct = $adjuncts_dy_from_boundings{ $whadjunct//'' };
  return "$x $y".( defined $adjunct and " $adjunct add translate" );
}

sub adjust_sea() {
  # Adjusts the top and bottom edges of the sea
  my $poly = $region{$c{Sea}}{Polygon};
  my %occurs;
  foreach my $v (@$poly) {
    $occurs{$v->{VxName}}++
  }
  foreach my $v (@$poly) {
    $v->{SeaOccurs} = $occurs{$v->{VxName}};
  }
  #print STDERR " sea ".(scalar @$poly)."\n";
  my $occurs_m = sub {
    my ($ei) = @_;
    $poly->[($ei+@$poly) % @$poly]{SeaOccurs} > 1
  };
  for my $ei (0..$#$poly) {
    next if $occurs_m->($ei);
    next unless $occurs_m->($ei-1);
    next unless $occurs_m->($ei+1);
    #print STDERR " sea would adjust $poly->[$ei]{VxName}\n";
    # adjust coord outwards
  }
  for my $ei (0..$#$poly) {
    next unless $occurs_m->($ei);
    #print STDERR " sea occurs_m $ei $poly->[$ei]{VxName}\n";
    next unless $occurs_m->($ei-1);
    my $delta = &mkvec(@{ $c{PA}{OuterCoastDelta} });
    foreach my $which (0,-1) {
      my $ej = ($ei +        $which + @$poly) % @$poly;
      my $ek = ($ei + +1 + 3*$which + @$poly) % @$poly;
      my $posj = \ $poly->[$ej]{Pos};
      my $posk = \ $poly->[$ek]{Pos};
      my $dout = $$posk - $$posj;
      #print STDERR " sea adj j=$ej ",prvec($$posj)," $poly->[$ej]{VxName}\n";
      #print STDERR " sea adj k=$ek ",prvec($$posk)," $poly->[$ek]{VxName}\n";
      foreach my $paocv (@{ $c{PA_Outercoast_Vertices} }) {
	if ($poly->[$ej]{VxName} =~ m{$paocv->{Re}}) {
	  #print STDERR " sea adj extra\n";
	  $$posk = $$posj + 10 * &mkvec(@{ $paocv->{PA} });
	}
      }
      $$posj = $$posj + $delta + $dout*10;
      $$posk = $$posk          + $dout*10;
    }
  }
}

sub psvec ($) {
  my ($pos) = @_;
  sprintf "%20.6f %20.6f", $pos->get(0),$pos->get(1)
}

sub face_path ($) {
  my ($rr) = @_;
  o("% $rr->{Name}\n");
  o("  newpath\n");
  my $xto = 'moveto';
  foreach my $vertex (@{ $rr->{Polygon} }) {
    my $pos = $vertex->{Pos};
    o(sprintf "    %s %s\n", psvec($pos), $xto);
    $xto = 'lineto';
  }
  o("    closepath\n");
}

sub computeboundings() {
  $bb{Entire} = [ 0,0, $a4_h, $a4_w + $a3_h ];
  my $submargins = sub {
    my ($margin, $pr) = @_;
    my @p = @$pr;
    [ $p[0] + $margin,
      $p[1] + $margin,
      $p[2] - $margin,
      $p[3] - $margin ];
  };
  my $somepage = sub {
    my $id = shift @_;
    print STDERR "defining page Page$id\n";
    $bb{"Page$id"} = [ @_ ];
    $bb{"Printable$id"} = $submargins->($max_printeredge, $bb{"Page$id"});
  };
  my $pageb_a4 = sub {
    $somepage->('B', 0,0, $a4_h, $a4_w );
  };
  if ($maxpaper eq 'a3') {
    $pageb_a4->();
    my $mt_offset = $bb{PrintableB}[3] - $max_printeredge;
    $somepage->('MT', 0, $mt_offset, $a4_h, $mt_offset + $a3_h );
    $bb{PrintableAll} = [
			 @{ $bb{PrintableB} }[0..1],
			 @{ $bb{PrintableMT} }[2..3],
			];
  } elsif ($maxpaper eq 'a4') {
    $pageb_a4->();
    my $m_offset = $bb{PrintableB}[3] - $max_printeredge;
    $somepage->('M', 0, $m_offset, $a4_h, $m_offset + $a4_w );
    my $t_offset = $bb{PrintableM}[3] - $max_printeredge;
    $somepage->('T', 0, $t_offset, $a4_h, $t_offset + $a4_w );
    $bb{PrintableAll} = [
			 @{ $bb{PrintableB} }[0..1],
			 @{ $bb{PrintableT} }[2..3],
			];
  } elsif ($maxpaper =~ m/^a1/) {
    my $offx = ($a1_w - $bb{Entire}[2] * (1 + $a1sfactor)) / 3;
    my $offy = 0.5*($a1_h - $bb{Entire}[3]);
    $somepage->('P', -$offx,-$offy, $a1_w-$offx, $a1_h-$offy);
    my $hairs = 30 * MM2PT;
    my $hairsw = $bb{Entire}[2];
    my $hairsh = $bb{Entire}[3];
    my $surround = 5 * MM2PT;
    my $surroundw = $bb{Entire}[2] + $surround*2;
    my $surroundh = $bb{Entire}[3] + $surround*2;
    my @hrect = @{ $bb{Entire} };
    if ($maxpaper eq 'a1m') {
      @hrect = @{ $submargins->($a1m_pasteedge, \@hrect) };
      $bb{Cutout} = \@hrect;
    }
    my $w_stroke = $xopts =~ m/W/ ?
	' gsave 3 setlinewidth stroke grestore '
	: '';
    $preamble_from_boundings = <<END;
         1 setlinewidth
         0 setgray
         /a1hairs {
             newpath moveto
             $hairs 0        rmoveto
             $hairs -2 mul 0 rlineto
             $hairs $hairs   rmoveto
             0 $hairs -2 mul rlineto
             stroke
         } def
         $hrect[0] $hrect[1] a1hairs
         $hrect[0] $hrect[3] a1hairs
         $hrect[2] $hrect[3] a1hairs
         $hrect[2] $hrect[1] a1hairs
         newpath
         -$surround -$surround moveto
          $surroundw 0         rlineto
          0 $surroundh         rlineto
          -$surroundw 0        rlineto
                 closepath $w_stroke clip
END
    $adjuncts_dy_from_boundings{Top}    = $c{PA}{BoundingsA1dy}[0];
    $adjuncts_dy_from_boundings{Bottom} = $c{PA}{BoundingsA1dy}[1];
    # ^ if we are doing it all in one go we lose less printable area
  } else {
    confess;
  }
}

sub showboundings () {
  return unless $xopts =~ m/B/;
  o(" gsave\n");
  my $bb = $bb{$bounding};
  o("  $bb->[0] neg $bb->[1] neg translate\n");
  my $i = 0;
  my $on = 2;
  my $off = 9;
  foreach my $bname (sort keys %bb) {
    o(sprintf "   1 %d %d setrgbcolor\n",
      !!($bname =~ m/^Page/),
      !!($bname =~ m/B$/),
     );
    o("    % $bname\n");
    o("    [ $on $off ] ".($i * ($on+$off))," setdash newpath\n");
    my @p = @{ $bb{$bname} };
    o("    $p[0] $p[1] moveto\n");
    o("    $p[2] $p[1] lineto\n");
    o("    $p[2] $p[3] lineto\n");
    o("    $p[0] $p[3] lineto\n");
    o("    closepath stroke\n");
    $i++;
  }
  o(" grestore");
}

sub o_amble (@) {
  # CPerl-mode does a really awful thing with %s in the preamble
  # and postamble, causing constant useless flashing
  # So we write & in the here docs and transform them back:
  my ($t) = join '', @_;
  $t =~ s/^\&+/ '%' x length $& /mge;
  o($t);
}

sub preamble() {
  my $bb = $bb{$bounding}; confess $bounding unless $bb;
  my $sz = [ $bb->[2] - $bb->[0], $bb->[3] - $bb->[1] ];
  o_amble(<<END);
&!PS-Adobe-3.0
&&BoundingBox 0 0 @$sz
&&Pages: 1
&&EndComments
&&BeginProlog
&&EndProlog
&&BeginSetup
<< /PageSize [ @$sz ] >> setpagedevice
/all {
  gsave
  $bb->[0] neg  $bb->[1] neg  translate
  $preamble_from_boundings
  board
  grestore
  showboundings
} def
/showboundings {
END
  showboundings();
  o_amble(<<END);
} def
/board {
  gsave
  % adjustment to actual image placement within prinable areas etc.:
  @{ $c{PA}{Overall} } translate
END
  my $oadj = $c{PA}{OverallOn}{$maxpaper};
  if ($oadj) {
    o("@$oadj translate\n");
  }
}

sub postamble () {
  o_amble(<<END);
  grestore
} def % board
&&EndSetup
&&Page: 1
&&BeginPageSetup
/pgsave save def
&&EndPageSetup
all
END
  if ($maxpaper =~ m/^a1/) {
    o(<<END);
  $a1_w 0 translate
  $a1sfactor dup scale
  $a1_w neg 0 translate
  1 1 3 { pop all 0 $a1_h 0.8 mul translate } for
END
  }
  o_amble(<<END);
pgsave restore
showpage
&&EOF
END
}

sub pscolour ($$$$) {
  my ($colourname, $cmykix,$rgbmul,$rgbadd) = @_;
  my $spec = $c{Colours}{$colourname};
  confess unless defined $spec;

  # check that we have only one lighter/darker retain value for each one
  our %colour_occurred;
  $colour_occurred{$colourname}{$cmykix}{$rgbmul,$rgbadd} = 1;
  keys %{ $colour_occurred{$colourname}{$cmykix} } == 1 or confess;

  my @r;
  my $rw;
  if ($spec =~ m/^[0-9a-f]+$/) {
    my $le = (length $spec)/3;
    my $re = ("(.{$le})") x 3;
    my @rgb = $spec =~ m/^$re$/;  @rgb or confess "$re ?";
    @rgb = map { hex($_) / (16**$le -1) } @rgb;
    foreach (@rgb) { $_ *= $rgbmul; $_ += $rgbadd; }
    @r = @rgb;
    $rw = 'setrgbcolor';
  } elsif ($spec =~ m/[CMYK]/) {
    my @specs = split /\//, $spec;
    if ($cmykix==1 && @specs==1) {
      $spec = $specs[0];
    } else {
      $spec = $specs[$cmykix];
      confess unless defined $spec;
    }
    my %cmyk;
    foreach (split /(?=[A-Z])/, $spec) {
      m/^([CMYK])([0-7](?:\.\d*)?|8)_?$/ or confess $_.' ?';
      confess if defined $cmyk{$1};
      $cmyk{$1} = $2 / 8.;
      #print STDERR " CMYK $1 $2\n";
    }
    @r = map { $cmyk{$_} // 0 } qw(C M Y K);
    $rw = 'setcmykcolor';
  } else {
    confess $spec.' ?';
  }
  my $r = join ' ',
      (map { sprintf "%.6f ", $_ } @r
      ),
	  $rw;
  return $r;
}

sub lighterpscolour ($$) {
  my ($colourname, $retain) = @_;
  #print STDERR "COLOUR LIGHTER $retain $colourname\n";
  pscolour($colourname, 0, $retain, 1-$retain);
}

sub darkerpscolour ($$) {
  my ($colourname, $retain) = @_;
  #print STDERR "COLOUR DARKER $retain $colourname\n";
  pscolour($colourname, 2, $retain, 0);
}

sub facepscolour ($) {
  my ($colourname) = @_;
  $xopts =~ m/W/ ? '1 setgray' :
      $colourname =~ m/land/ ? miscpscolour($colourname)
      : lighterpscolour($colourname, 0.75);
}

sub miscpscolour ($) {
  my ($colourname) = @_;
  #print STDERR "COLOUR MISC $colourname\n";
  pscolour($colourname, 1, 1,0);
}

sub seatrackedgepscolour () { darkerpscolour('cube', 0.25) }

sub fill_faces() {
  o("clippath ", facepscolour('land'), " fill\n");
  foreach my $rr (values %region) {
    next if $rr->{Name} =~ m/^L2?$/;
    face_path($rr);
    o(" ", facepscolour($rr->{Colour}), " eofill\n");
  }
}

our $edge_lw = 5;

sub some_edge_faces (@) {
  o("$edge_lw setlinewidth 1 setlinejoin ",miscpscolour('edge'),"\n");
  foreach my $rr (@_) {
    next if $rr->{Name} =~ m/^L2?$/;
    face_path($rr);
    o("    stroke\n");
  }
}

sub dashedwithhs ($$$) {
  my ($inner, $comment, $colour) = @_;
  my ($adj) = grep { $_->{Name} eq $c{Sea} } @{ $region{$inner}{Adj} };
  my $dash = 20;
  my (@pos) = adj_ends($adj);
  my $mid = 0.5 * ($pos[0] + $pos[1]);
  my $r = 0.5 * ($pos[1] - $pos[0])->norm();
  o(sprintf <<'END', $comment, facepscolour('sea'), psvec($mid), $r);
  %% %s
  %s %s %s 0 360 arc closepath fill
END
  some_edge_faces(()); # just sets the colour
  o(sprintf <<END, map { psvec($_) } @pos);
  newpath %s moveto %s lineto
  [ $dash $dash ] $dash 0.5 mul setdash
  stroke
  [] 0 setdash
END
  one_hs($colour, $mid);
}

sub dashedwithhses() {
  dashedwithhs(
	      $_->{Inner},
	      $_->{Comment},
	      $_->{Colour}
	     ) foreach @{ $c{DashedHs} };
}

sub edge_faces() {
  some_edge_faces($region{$c{Sea}});
  dashedwithhses();
  some_edge_faces(grep {
    my $n = $_->{Name};
    $n !~ m{^$c{OuterfaceRe}$} &&
    !grep { $n eq $_->{Inner} } @{ $c{DashedHs} }
  } values %region);
}

our $hs_sz = 17 * MM2PT;
our $hs_lw = 3;

our $text_sz = 18;

sub token_circle ($$$$;$$) {
  my ($what, $pos, $fillcolour, $edgecolour, $sz,$lw) = @_;
  $sz //= $hs_sz;
  $lw //= $hs_lw;
  o(sprintf <<END, $what, $fillcolour, psvec($pos), $sz/2, $edgecolour);
   %% %s
   newpath %s
     %s %f 0 360 arc closepath gsave fill grestore
     %s $lw setlinewidth stroke
END
}

sub one_hs ($$;$$) {
  my ($colour, $hspos, $sz, $lw) = @_;
  token_circle("HS - $colour",
	       $hspos,
	       darkerpscolour($colour, 0.75),
	       miscpscolour('edge'),
	      $sz,$lw);
}

our $cube_sz = 6 * MM2PT;
our $cube_gap = $cube_sz * 0.2;
our $cube_lw = ($cube_sz * 0.1);

sub label_faces() {
  my $sz = $text_sz;
  my $shadow = $sz * 0.1;
  my $textc   = miscpscolour('text');
  my $shadowc = miscpscolour('textshadow');
  o(<<END);
  /wordsshow {
    $shadowc 0 10 359 {
      dup  cos $shadow mul
      exch sin $shadow mul
      words pop pop
    } bind for
    $textc 0 0 words pop pop
  } def
END
  o("  /Helvetica-Bold findfont  $sz scalefont setfont\n");
  my %hs = map { $_ => 1 } @{ $c{HS} };
  foreach my $rr (values %region) {
    next if $rr->{Name} =~ m/^L\d?$/;
    my $words = $rr->{DisplayName};
    my $pos;
    if ($rr->{Name} eq $c{Sea}) {
      $pos = mkvec(90, 1280);
    } else {
      $pos = $rr->{Centre} + mkvec( 0, 0.5 * $sz * @$words );
    }
    if ($hs{$rr->{Name}}) {
      #print STDERR "adjust hs $rr->{Name}\n";
      my $posadj = $c{PA_HS_HS}{$rr->{Colour}};
      $posadj //= [0,0];
      $pos -= mkvec(0, -$hs_sz * 0.65);
      $pos += &mkvec(@$posadj) * $hs_sz;
      my $hspos = $pos - mkvec(0, $sz * @$words + $hs_sz/2 + $hs_lw
			       + $hs_sz*0.15);
      one_hs($rr->{Colour}, $hspos);
    }
    my $adjust = $c{TextAdjust}{$rr->{Name}};
    if ($adjust) {
      $pos += $sz * &mkvec(@$adjust);
    }
    my $w = $rr->{Water};
    $w = 2 if $rr->{Name} eq $c{Sea};
    if ($w) {
      #print STDERR " water $w in $rr->{Name}\n";
      my $down = water_cubes_sz(2) - water_cubes_sz(1) + $cube_gap;
      $pos -= mkvec(0, -0.5 * $down);
      my $cube_y = -($sz * @$words);
      if (grep { $_ eq $rr->{Name} } @{ $c{CubeAbove} }) {
	$pos -= mkvec(0,+$sz);
	$cube_y = 0;
	$down = 0;
      }
      o("  gsave\n");
      o("    ", psvec($pos), " translate\n");
      o("    0 ", $cube_y, " translate\n");
      o("    ", -0.5 * water_cubes_sz($w)," ", -$down, " translate\n");
      water_cubes($w, miscpscolour('edge'));
      o("  grestore\n");
    }
    o("  /words {\n");
    foreach my $word (@$words) {
      $pos += mkvec(0,-$sz);
      o("   ", psvec($pos), " moveto");
      o(<<END);
   2 copy rmoveto
   ($word) dup stringwidth pop     % word xw
     -0.5 mul 0 rmoveto             % word
     show
   0 -$sz rmoveto
END
    }
    o(<<END);
  } bind def
  wordsshow
END
  }
}

sub water_cubes_sz($){
  my ($n) = @_;
  ($cube_sz + $cube_lw) * $n + ($cube_gap * ($n-1));
}
  
sub water_cubes($$){
  my ($n, $edgecolour) = @_;
  my $s = $cube_sz;
  for my $i (0..$n-1) {
    o("  newpath  ", $i * ($cube_sz + $cube_lw + $cube_gap),"  0  moveto\n");
    o("          $s  0  rlineto\n");
    o("           0 $s  rlineto\n");
    o("         -$s  0  rlineto  closepath\n");
    o("  gsave ", miscpscolour('cube'), " fill grestore\n");
    o("  gsave ", $edgecolour, " ", $cube_lw,
      " setlinewidth stroke grestore \n");
  }
}

my $deck_h = 89 * MM2PT;

sub decks(){
  my $lw = $edge_lw/2;
  o("  /deck1path {\n");
  my $w = 63 * MM2PT;
  my $h = $deck_h;
  my $r =  5 * MM2PT;
  o("      ",($w/2)," 0 moveto\n");
  my @p = ([ $w, 0 ],
	   [ $w,$h ],
	   [ 0, $h ],
	   [ 0,  0 ]);
  foreach my $i (0..3) {
    o("    @{ $p[$i] }  @{ $p[($i + 1) % 4] }  $r  arct\n");
  }
  o("    closepath\n");
  o("  } bind def");

  o("  /deckpairpathx {\n"); # x because it leaves matrix changed
  o("    newpath         deck1path\n");
  o("    200 0 translate deck1path\n");
  o("  } bind def");

  foreach my $deck (@{ $c{Decks} }) {
    o("  $lw setlinewidth ",miscpscolour($deck->{Colour}),"\n");
    o("  gsave ".pos_plus_dy_adjunct($deck->{PosAdy})."     ");
    my $rotate = $deck->{Rotate};
    if ($rotate) { o(" $rotate rotate "); }
    o("deckpairpathx stroke grestore");
  }
}

sub sea_track(){
  o("% sea level track\n");
  o("  gsave ",pos_plus_dy_adjunct($c{PA}{SeaTrack})," translate\n");
  my $track = $c{SeaTrack};
  my $token_sz = $hs_sz;
  my $dy = $token_sz * 1.25;
  my $dx = $token_sz * 1.25;
  my $linecolour = seatrackedgepscolour();
  my @pos;
  for my $i (0..$#$track) {
    $pos[$i] = mkvec(0, ($i - 0.5 * @$track) * $dy);
  }
  my $prepline = "$linecolour $hs_lw setlinewidth newpath";
  o("  $prepline\n");
  o("    ",psvec($pos[0])," moveto ",psvec($pos[-1])," lineto stroke\n");
  for my $pos (@pos) {
    token_circle("SL", $pos,
		 miscpscolour('cube'),
		 $linecolour);
  }
  my $last = 0;
  my $cubeedgecolour = seatrackedgepscolour();
  for my $i (0..$#pos) {
    my $differs = $track->[$i] != $last;
    $last = $track->[$i];
    next unless $differs;
    o("  $prepline\n");
    o("  gsave ",
      psvec($pos[$i] + mkvec(-0.0 * $token_sz, $dy/2))," translate\n");

    my $t_sz = 24;
    my $shrink = ($hs_sz * 0.75 / $deck_h);
    o("    gsave % T M $i\n");
    o("      -43.5 -6.5 translate\n");
    o("      0.8 .8 scale \n");
    o("      ",0.6 * $dx, " ", -0.28 * $dy," translate\n");
    o("      gsave");
    o("        $shrink dup scale -90 rotate\n");
    o("        newpath ", ($hs_lw / $shrink)," setlinewidth\n");
    o("         deck1path stroke\n");
    o("      grestore");
    o("      /Helvetica-Bold findfont $t_sz scalefont setfont\n");
    o("      12 -21.5 moveto ($track->[$i]) show");
    o("    grestore\n");

    if ($i > 0) {
      o("  0 -$dy moveto",
	" ",psvec(mkvec( $dx + $cube_sz, 0 )),
	" rlineto stroke\n");
    }
    o("  ",($token_sz * 0.6 + $cube_gap)," ",
      (-$dy * 0.5 - 0.5 * (water_cubes_sz(2))),
      " translate\n");
    my $bottom = int(($track->[$i+1] + 1) / 2);
    water_cubes($bottom, $cubeedgecolour);
    my $new_n = $track->[$i+1] - $bottom;
    o("  ", 0.5 * (water_cubes_sz($bottom) - water_cubes_sz($new_n)),
      "  ",       (water_cubes_sz(2)-water_cubes_sz(1)), " translate\n");
    water_cubes($new_n, $cubeedgecolour);
    o("  grestore\n");
  }
  o("  grestore\n");
}

our $dyke_l = 25 * MM2PT;;

sub dykes(){
  my $l = $dyke_l;
  my $w = 5 * MM2PT;
  my $lw = $edge_lw * 0.50;
  my $l2 = $l/2;
  my $w2 = $w/2;
  o("  /dykepath {\n");
  o("            -$l2 -$w2 moveto\n");
  o("             $l2 -$w2 lineto\n");
  o("             $l2  $w2 lineto\n");
  o("            -$l2  $w2 lineto closepath\n");
  o("  } bind def\n");
  o("  /dyke {\n");
  o("    newpath dykepath\n");
  o("    gsave ",miscpscolour('dykefill')," fill grestore\n");
  o("    gsave ",miscpscolour('edge')," $lw setlinewidth stroke grestore\n");
  o("  } bind def\n");
  my $symbol_sz = $w * 1.50;
  my $t_sz = $symbol_sz * 1.0;
  my $t_at = sub {
    my ($ang) = @_;
    #print STDERR "ANG $ang\n";
    map { $t_sz * 0.5 * $_ } cos($ang), sin($ang);
  };
  my @t0 = $t_at->((TAU) *  0.25         );
  my @t1 = $t_at->((TAU) * (0.25 + 1./3) );
  my @t2 = $t_at->((TAU) * (0.25 + 2./3) );
  o("  /dtriangle {\n");
  o("    newpath @t0 moveto\n");
  o("            @t1 lineto\n");
  o("            @t2 lineto closepath\n");
  o("    gsave ",miscpscolour('dykeinit')," fill grestore\n");
  o("    gsave ",miscpscolour('edge')," $lw setlinewidth stroke grestore\n");
  o("  } bind def\n");
  foreach my $rr (values %region) {
    next if $rr->{Special};
    next if grep { $_->{Inner} eq $rr->{Name} } @{ $c{DashedHs} };
    next if $rr->{Colour} eq 'high';
    foreach my $adj (@{ $rr->{Adj} }) {
      my $neigh = $adj->{Name};
      o("  % dyke $rr->{Name} --- $neigh\n");
      next if $neigh lt $rr->{Name} && !$region{$neigh}{Special};
      next if $region{$neigh}{Colour} eq 'high';
      my @ends = adj_ends($adj);
      my $dirn = $ends[1] - $ends[0];
      my $angle = vec2atan($dirn);
      $angle = $angle * 360 / TAU;
      o("  gsave\n");
      o("    ", psvec( 0.5 * ($ends[0] + $ends[1]) )," translate\n");
      o("    $angle rotate\n");
      my $initial = $adj->{Dykes};
      for my $ys ($initial
		  ? [ map { $_ - 0.5 * ($initial-1) } 0..$initial-1 ]
		  : [0]) {
	foreach my $y (@$ys) {
	  o("    gsave 0 ", $y * $w, " translate dyke ");
	  o(" grestore\n");
	}
	my $nsymbols = $initial + !!$adj->{Deltawerk};
	o("   ", -0.5 * $symbol_sz * ($nsymbols-1)," 0 translate\n");
	my $trans = "  $symbol_sz 0 translate";
	if ($adj->{Deltawerk}) {
	  one_hs('red', mkvec(0,0), $symbol_sz * 0.8, $lw);
	  o($trans);
	}
	foreach my $i (1..$initial) {
	  o(" gsave $angle neg rotate dtriangle grestore\n");
	  o($trans);
	}
      }
      o("  grestore\n");
    }
  }
}

sub hs_initial_1 ($) {
  my ($hs1) = @_;
  my ($colour) = $hs1->{Colour};
  my ($text, $text2);
  if ($hs1->{Water}) {
    ($text,$text2) = ('up to '.$hs1->{Water}, 'fewer');
  } elsif ($hs1->{Dykes}) {
    ($text,$text2) = ('up to '.$hs1->{Dykes}, '');
  }
  my $interval = $hs_sz * 1.75;
  my $demo_h = $hs_sz; #water_cubes_sz(3);
  my $demo_x0 = $hs_sz * 0.75 + $dyke_l * 0.60;
  my $demo_w = $demo_x0 + $dyke_l * 0.60;
  o("  % HS I $colour\n");
  o("  gsave\n");
  o("    ".pos_plus_dy_adjunct($c{PA}{HS1_initials})."\n");
  o("    ", ($hs1->{XIYI}[0]*$interval), " ",
           (($hs1->{XIYI}[1]-1)*$interval), " translate\n");
  if (defined $text) {
    o("  gsave\n");
    o("        $demo_h setlinewidth 0 0 moveto $demo_w 0 rlineto\n");
    o("        ", facepscolour($colour), " stroke grestore\n");
  }
  one_hs($colour, mkvec(0,0));
  if (defined $text) {
    o("    /words {\n");
    o("      2 copy moveto                      ($text) show\n");
    o("      2 copy moveto 0 -$text_sz rmoveto ($text2) show\n");
    o("    } bind def\n");
    o("    gsave ".($hs_sz * 0.8)." $cube_gap translate wordsshow grestore\n");
    o("    $demo_x0 ",($hs_sz * -0.25)," translate\n");
  }
  if ($hs1->{Water}) {
    o("    ",(water_cubes_sz(1))," 0 translate\n");
    o("    0 ",(-water_cubes_sz(1) * 0.3)," translate\n");
    water_cubes(1, miscpscolour('edge'));
  } elsif ($hs1->{Dykes}) {
    o("    newpath dykepath\n");
    o("    gsave ",miscpscolour('edge')," stroke grestore\n");
    o("    gsave ",miscpscolour('dykehsadd')," fill grestore\n");
  }
  o("  grestore\n");
}

sub hs_initial () {
  hs_initial_1($_) foreach @{ $c{HS1} };
}

sub play_hint () {
  my $sz = 20;
  o("  gsave ",miscpscolour('hinttext'),
    " ",pos_plus_dy_adjunct($c{PA}{PlayHint}),"\n");
  o("  /Helvetica-Bold findfont $sz scalefont setfont\n");
  my @t = split /\n/, $c{PlayHint};
  foreach my $i (0..$#t) {
    my $t = $t[$i];
    $t =~ s/[\\()]/\\$&/g;
    o("  0 ",(-$i * $sz)," moveto ($t) show\n");
  }
  o("  grestore");
}

#----- main program -----

$xopts = shift @ARGV if $ARGV[0] =~ s/^-X//;
$bounding = $1 if $xopts =~ s/b(\w+)//;
$maxpaper = $1 if $xopts =~ s/p(\w+)//;
$maxpaper =~ m/^a[134]$|^a1m$/ or confess;

($facesf, @ARGV) = @ARGV or die;
parse_input_graph();
prep_region_rmap();
read_faces();
calculate_centres();
transform_coordinates();
adjust_sea();

computeboundings();
preamble();
fill_faces();
edge_faces();
dykes();
label_faces();
decks();
sea_track();
hs_initial();
play_hint();
postamble();

print STDERR Dumper(\%region) if $ENV{'GENERATE_BOARD_DUP'};

# Local variables:
# cperl-indent-level: 2
# End.
