# misc-data.pl - details from Pandemic Rising Tide needed to draw a board
#
# Very probably copyright (C) 2017 Z-Man Games
# Copyright (C) 2019 Ian Jackson
#
# This file is probably, in copyright law, a derivative work of
# Pandemic Rising Tide, because it gives many details about the game
# and even some text derived from that on the orginal board.  It
# also contains many consequential details added by me, Ian Jackson.
#
# My own contributions are dual licensed, GPv3+ or CC-BY 4.0+,
# Only to the Pandemic Rising Tide folks, it is permissively licensed.
#
# Pandemic and Pandemic Rising Tide are (I think) trademarks of Z-Man
# games and I use them without permission.


our %c =
(
 OrientRegions => [ 'Roer en Overmaas', 'Noorderzijlvest' ],
 OrientBearing => -15,
 GraphScale => 30,
 Colours => {
     yellow => 'fd0',
     purple => 'b1f',
     green  => '0a0',
     red    => 'f50',
     high   => '888',
     sea    => '3ad',
     cube   => '8df',
     land   => '987654', # pale brown (WP)
#     land   => 'f4a460', # sandy brown
#     land   => '8b4513', # saddlebrown
#     land   => 'a0522d', # sienna
     edge   => '000',
     text        => 'fff',
     textshadow  => '000',
     dykefill    => '7ff7ff7ff',
     dykeinit    => 'fff',
     dykehsadd   => '642',
     dykefail => '444',
     playerdeck => 'fff',
     hinttext   => '000',
 },
 Sea => 'NZ',
 OuterfaceRe => qr{(?:NZ|L2?)},
 SpecialRegions => [qw(NZ L L2)],
 DisplayNames => {
     ZZ => 'Zuiderzee',
     NZ => 'Noordzee',
 },
 HS => [ 'Roer en Overmaas', 'Schouwen-Duiveland', 'Rijn en IJssel' ],
 DashedHs => [ { 
     Inner => 'ZZ',
     Comment => 'Afsluitdijk',
     Colour => 'purple',
 } ],
 SeaTrack => [qw(2 2 2 3 3 3 4 4 4)],
 CubeAbove => [
   'Wieringermeer'
 ],
 TextAdjust => {
   'Zeeuws-Vlaanderen' => [0.6, 0],
   'Walcheren'         => [0.6, 0.5], #-1.0, -0.75
   'Noorderzijlvest'   => [0.0, -0.5],
   'Twente',           => [-0.3, 0.0],
   'Land van Heusden', => [-0.3, 0.0],
   'Land van Altena',  => [-0.3, 0.0],
   'Voorne-Putten',    => [+0.3, 0.0],
   'Wieringermeer',    => [+0.2, -0.2],
 },
 PlayHint => <<'END',
1. Do 4 Actions
2. Operate Pumps
3. Draw 2 player cards
     (resolve storms)
4. Dykes fail
5. Water flows
Hand limit 7 at all times
END
 PA => {
   OuterCoastDelta => [ -1000, 0 ],
   BoundingsA1dy => [ 35, -7 ],
   Overall => [ 34, -15 ],
   OverallOn => { a4 => [ 0, -17 ] },
   HS1_initials => [ 550, 1705, 'Top' ],
   SeaTrack => [ 11, 700 ],
   PlayHint => [ 260, 1715, 'Top' ],
 },
 PlayHintdyAdjunct => 'Top',
 PA_HS_HS => { green => [0, -0.40] },
 PA_Outercoast_Vertices => [
   { Re => qr{Noorderzijlvest}, PA => [ 100,-50 ] },
 ],
 Decks => [
   { Colour => 'playerdeck', PosAdy => [ 401, 44, 'Bottom' ] },
   { Colour => 'dykefail', PosAdy => [ 242, 1363, 'Top'], Rotate => 90 },
 ],
 HS1 => [
   { Colour => 'red'    , XIYI => [0,0] },
   { Colour => 'purple' , XIYI => [0,1] },
   { Colour => 'green'  , XIYI => [1,0], Water => 6 },
   { Colour => 'yellow' , XIYI => [1,1], Dykes => 4 },
 ],
);
